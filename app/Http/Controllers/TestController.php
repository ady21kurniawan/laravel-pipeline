<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestController extends Controller
{
    public function index()
    {
        echo "result from test controller";
    }

    public function forTesting()
    {
       return json_encode(["success"=>true, "message"=>"success", "error"=>null]);
    }
}
